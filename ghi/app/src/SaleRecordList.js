import { useEffect, useState } from "react";



const SaleRecordList = () => {
    const [sale_records, setRecords] = useState([]);

    useEffect(() => {
        fetch('http://localhost:8090/api/sale_records/')
            .then(response => response.json())
            .then(data => {
                setRecords(data.sale_records);
            })
            .catch(e => console.log('error: ', e));
    }, [])

    return (
      <>
        <h1>Sale Records</h1>
        <table className="table table-striped">
            <thead>
            <tr>
                <th>Automobile Vin</th>
                <th>Customer</th>
                <th>Salesperson</th>
                <th>Price</th>
            </tr>
            </thead>
            <tbody>
            {sale_records && sale_records.map(record => {
                return (
                <tr key={record.id}>
                    <td>{record.automobile.vin}</td>
                    <td>{record.customer.customer_name}</td>
                    <td>{record.salesperson.name}</td>
                    <td>{record.price}</td>
                </tr>
                );
            })}
            </tbody>
        </table>
        <button onClick={() => window.location.href="http://localhost:3000/records/new"}>Add a New Sale</button>
      </>
    );
  }

  export default SaleRecordList;
