import React, { useEffect, useState } from 'react';
import { Link } from "react-router-dom";

const CustomerForm = () => {

    const [customerName, setCustomerName] = useState('');
    const [customerPhone, setCustomerPhone] = useState('');
    const [customerAddress, setCustomerAddress] = useState('');

    const handleSubmit = (event) => {
        event.preventDefault();
        const newCustomer = {
            'customer_name': customerName,
            'phone_number': customerPhone,
            'address': customerAddress,
        }

        const customersUrl = 'http://localhost:8090/api/customers/';
        const fetchConfig = {
        method: "post",
        body: JSON.stringify(newCustomer),
        headers: {
            'Content-Type': 'application/json',
            },
        };
        fetch(customersUrl, fetchConfig)
            .then(response => response.json())
            .then(() => {
                setCustomerName('');
                setCustomerPhone('');
                setCustomerAddress('');
                window.location.href=`http://localhost:3000/customers/`
        })
            .catch(e => console.log('error: ', e));
    }


    const handleNameChange= (event) => {
        const value = event.target.value;
        setCustomerName(value);
    }
    const handleNumChange= (event) => {
        const value = event.target.value;
        setCustomerPhone(value);
    }

    const handleAddressChange= (event) => {
      const value = event.target.value;
      setCustomerAddress(value);
  }

    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a Valued Customer!</h1>
            <form onSubmit={handleSubmit} id="create-customer-form">
                <div className="form-floating mb-3">
                <input value={customerName} onChange={handleNameChange} placeholder="customer_name" required type="text" name="customer_name" id="customer_name" className="form-control" />
                <label htmlFor="customer_name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={customerPhone} onChange={handleNumChange} placeholder="phone_number" required type="text" name="phone_number" id="phone_number" className="form-control" />
                <label htmlFor="phone_number">Phone Number</label>
              </div>
              <div className="form-floating mb-3">
                <input value={customerAddress} onChange={handleAddressChange} placeholder="address" required type="text" name="address" id="address" className="form-control" />
                <label htmlFor="address">Address</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
            <Link to={"/salespeople"}>Return to Customer List</Link>
          </div>
        </div>
      </div>
    );
  }

export default CustomerForm;
