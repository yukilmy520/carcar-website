import { BrowserRouter, Routes, Route, useNavigate} from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ManufacturerForm from './ManufacturerForm';
import ManufacturersList from './ManufacturerList';
import VehicleForm from './VehicleForm';
import VehicleList from './VehicleList';
import AutomobileForm from './AutomobileForm';
import AutomobileList from './AutomobileList';
import SalespeopleList from './SalespeopleList';
import SalespersonForm from './NewSalesperson';
import CustomerForm from './CustomerForm';
import CustomerList from './CustomerList';
import SaleRecordForm from './SaleRecordForm';
import SaleRecordList from './SaleRecordList';
import SalespersonRecordList from './EmployeeRecordList';
import TechnicianForm from './TechnicianForm';
import TechnicianList from './TechnicianList';
import AppointmentForm from './AppointmentForm';
import AppointmentList from './AppointmentList';
import AppointmentHistoryList from './AppointmentHistory';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="manufacturers">
            <Route index element={<ManufacturersList />} />
            <Route path="new" element={<ManufacturerForm />} />
          </Route>
          <Route path="automobiles">
            <Route index element={<AutomobileList  />} />
            <Route path="new" element={<AutomobileForm />} />
          </Route>
          <Route path="vehicles">
            <Route index element={<VehicleList  />} />
            <Route path="new" element={<VehicleForm />} />
          </Route>
          <Route path="salespeople">
            <Route index element={<SalespeopleList  />} />
            <Route path="new" element={<SalespersonForm />} />
          </Route>
          <Route path="customers">
            <Route index element={<CustomerList  />} />
            <Route path="new" element={<CustomerForm />} />
          </Route>
          <Route path="records">
            <Route index element={<SaleRecordList  />} />
            <Route path="filter" element={<SalespersonRecordList />} />
            <Route path="new" element={<SaleRecordForm />} />
          </Route>
          <Route path="technicians">
            <Route index element={<TechnicianList />} />
            <Route path='new' element={<TechnicianForm />} />
          </Route>
          <Route path="appointments">
            <Route index element={<AppointmentList />} />
            <Route path='new' element={<AppointmentForm />} />
            <Route path='history' element={<AppointmentHistoryList />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
