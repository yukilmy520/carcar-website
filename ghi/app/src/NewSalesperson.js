import React, { useEffect, useState } from 'react';
import { Link } from "react-router-dom";

const SalespersonForm = () => {

    const [employeeName, setEmployeeName] = useState('');
    const [employeeNumber, setEmployeeNum] = useState('');

    const handleSubmit = (event) => {
        event.preventDefault();
        const newSalesperson = {
            'name': employeeName,
            'employee_num': employeeNumber,
        }

        const salespeopleUrl = 'http://localhost:8090/api/salespeople/';
        const fetchConfig = {
        method: "POST",
        body: JSON.stringify(newSalesperson),
        headers: {
            'Content-Type': 'application/json',
            },
        };
        fetch(salespeopleUrl, fetchConfig)
            .then(response => response.json())
            .then(() => {
                setEmployeeName('');
                setEmployeeNum('');
                window.location.href=`http://localhost:3000/salespeople/`
        })
            .catch(e => console.log('error: ', e));
    }


    const handleNameChange= (event) => {
        const value = event.target.value;
        setEmployeeName(value);
    }
    const handleNumChange= (event) => {
        const value = event.target.value;
        setEmployeeNum(value);
    }

    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add Salesperson</h1>
            <form onSubmit={handleSubmit} id="create-salesperson-form">
                <div className="form-floating mb-3">
                <input value={employeeName} onChange={handleNameChange} placeholder="name" required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={employeeNumber} onChange={handleNumChange} placeholder="employee_id" required type="text" name="employee_id" id="employee_id" className="form-control" />
                <label htmlFor="employee_id">Employee Id</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
            <Link to={"/salespeople"}>Return to Salespeople List</Link>
          </div>
        </div>
      </div>
    );
  }

export default SalespersonForm;
