import { NavLink, Link } from "react-router-dom";
import "./index.css";

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-secondary">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">
          CarCar
        </NavLink>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/">
                Home
              </NavLink>
            </li>
            <li className="nav-item dropdown">
              <Link
                className="nav-link dropdown-toggle active"
                id="navbarDropdown"
                data-bs-toggle="dropdown"
                to="/manufacturers"
              >
                Inventory
              </Link>
              <ul className="dropdown-menu dropdown-menu-right">
                <li>
                  <Link className="dropdown-item" to="/manufacturers">
                    Manufacturers
                  </Link>
                </li>
                <li>
                  <Link className="dropdown-item" to="/vehicles">
                    Vehicles
                  </Link>
                </li>
                <li>
                  <Link className="dropdown-item" to="/automobiles">
                    Automobiles
                  </Link>
                </li>
              </ul>
            </li>

            <li className="nav-item dropdown">
              <Link
                className="nav-link dropdown-toggle active"
                id="navbarDropdown"
                data-bs-toggle="dropdown"
                to="/salespeople"
              >
                Sales
              </Link>
              <ul className="dropdown-menu dropdown-menu-right">
                <li>
                  <Link className="dropdown-item" to="/salespeople">
                    Salespeople
                  </Link>
                </li>
                <li>
                  <Link className="dropdown-item" to="/customers">
                    Customers
                  </Link>
                </li>
                <li>
                  <Link className="dropdown-item" to="/records">
                    Sale Records
                  </Link>
                </li>
                <li>
                  <Link className="dropdown-item" to="/records/filter">
                    Salesperson Sale Records
                  </Link>
                </li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <Link
                className="nav-link dropdown-toggle active"
                id="navbarDropdown"
                data-bs-toggle="dropdown"
                to="/appointments"
              >
                Service
              </Link>
              <ul className="dropdown-menu dropdown-menu-right">
                <li>
                  <Link className="dropdown-item" to="/appointments">
                    Appointments List
                  </Link>
                </li>
                <li>
                  <Link className="dropdown-item" to="/appointments/new">
                    Add New Appointment
                  </Link>
                </li>
                <li>
                  <Link className="dropdown-item" to="/appointments/history">
                    {" "}
                    Service History
                  </Link>
                </li>
                <li>
                  <Link className="dropdown-item" to="/technicians">
                    Technicians List
                  </Link>
                </li>
                <li>
                  <Link className="dropdown-item" to="/technicians/new">
                    Add New Technician
                  </Link>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}
export default Nav;
