import React from 'react';
import { useNavigate } from "react-router-dom";

class AutomobileForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
        color: '',
        year:'',
        vin:'',
        model_id:'',
        is_sold: false,

    };
    this.handleColorChange = this.handleColorChange.bind(this);
    this.handleYearChange = this.handleYearChange.bind(this);
    this.handleVinChange = this.handleVinChange.bind(this);
    this.handleModelsChange = this.handleModelsChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state};
    delete data.models;


    const automobileUrl = 'http://localhost:8100/api/automobiles/';
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(automobileUrl, fetchConfig);
    if (response.ok) {
      const newAutomobile = await response.json();

      const cleared = {
        color: '',
        year:'',
        vin:'',
        model_id:'',
      };
      this.setState(cleared);
      window.location.href='http://localhost:3000/automobiles'
    }
  }

  handleColorChange(event) {
    const value = event.target.value;
    this.setState({color: value})
  }

  handleYearChange(event) {
    const value = event.target.value;
    this.setState({year: value})
  }

  handleVinChange(event) {
    const value = event.target.value;
    this.setState({vin: value})
  }

  handleModelsChange(event) {
    const value = event.target.value;
    this.setState({model_id: value})
  }

  async componentDidMount() {
    const url = `http://localhost:8100/api/models/`;

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      this.setState({models: data.models});
    }
  }


  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add an automobile to inventory</h1>
            <form onSubmit={this.handleSubmit} id="create-hats-form">
              <div className="form-floating mb-3">
                <input value={this.state.color} onChange={this.handleColorChange} placeholder="color" required type="text" name="color" id="color" className="form-control" />
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input value={this.state.year} onChange={this.handleYearChange} placeholder="year" required type="text" name="year" id="year" className="form-control" />
                <label htmlFor="year">Year</label>
              </div>
              <div className="form-floating mb-3">
                <input value={this.state.vin} onChange={this.handleVinChange} placeholder="vin" required type="text" name="vin" id="vin" className="form-control" />
                <label htmlFor="vin">Vin</label>
              </div>
              <div className="mb-3">
                <select value={this.state.model} onChange={this.handleModelsChange } required name="model" id="model" className="form-select">
                  <option value="">Choose a model</option>
                  {this.state.models&&this.state.models.map(model => {
                    return (
                      <option key={model.id} value={model.id}>
                        {model.name}
                      </option>
                    );
                  })}
                </select>
              </div>

              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default AutomobileForm;
