import { useEffect, useState } from "react";
import { Link } from "react-router-dom";

const AutomobilesList = () => {
    const [autos, setAutomobiles] = useState([]);

    useEffect(() => {
        fetch('http://localhost:8100/api/automobiles/')
            .then(response => response.json())
            .then(data => {
                setAutomobiles(data.autos);
            })
            .catch(e => console.log('error: ', e));
    }, [])

    return (
        <>
            <br/>
            <h1>Vehicle models</h1>
            <table className="table table-striped">
                <thead>
                <tr>
                    <th>Vin</th>
                    <th>Color</th>
                    <th>Year</th>
                    <th>Model</th>
                    <th>Manufacturer</th>
                    <th>Status</th>
                </tr>
                </thead>
                <tbody>
                    {autos && autos.map(auto => {
                        if (auto.is_sold === true) {
                            auto['is_sold'] = 'Sold!'
                        } else if ( auto.is_sold === false ) {
                            auto['is_sold'] = 'In Stock!'
                        }
                        return (
                            <tr key={auto.href}>
                                <td >{ auto.vin }</td>
                                <td>{ auto.color }</td>
                                <td >{ auto.year }</td>
                                <td >{ auto.model.name }</td>
                                <td >{ auto.model.manufacturer.name }</td>
                                <td>{auto.is_sold}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
            <Link to={"/automobiles/new"} className="link-color">Add Automobiles</Link>
        </>
    );
  }

  export default AutomobilesList;
