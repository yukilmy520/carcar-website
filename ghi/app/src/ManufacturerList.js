import { useEffect, useState } from "react";
import { Link } from "react-router-dom";


const ManufacturerList = () => {
    const [manufacturers, setManufactures] = useState([]);

    useEffect(() => {
        fetch('http://localhost:8100/api/manufacturers/')
            .then(response => response.json())
            .then(data => {
                setManufactures(data.manufacturers);
            })
            .catch(e => console.log('error: ', e));
    }, [])

    return (
      <>
        <br/>
        <h1>Manufacturers</h1>
        <table className="table table-striped">
            <thead>
            <tr>
                <th>Name</th>
            </tr>
            </thead>
            <tbody>
            {manufacturers && manufacturers.map(manufacturer => {
                return (
                <tr key={manufacturer.href}>
                    <td>{manufacturer.name }</td>
                </tr>
                );
            })}
            </tbody>
        </table>
        <Link to={"/manufacturers/new"} className="link-color">Add manufacturers</Link>
      </>
    );
  }

  export default ManufacturerList;
