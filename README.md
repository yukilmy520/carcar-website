# Welcome to CarCar!
This is a car dealership management app that offers the best solution for car dealership management!
It is the use of both the backend (Django) and frontend (REACT) frameworks to create a single page application with multiple microservices based on a car dealership. A fully functional scaffold of microservices, a front-end application, and a database are included with the starter application. The data from all of the microservices will be stored in a PostgreSQL database.

## How to Run this Application:

1.git clone this project to your local repository;

2.run the docker by typing "docker volume create `beta-data`,`docker-compose build` and `docker-compose up` commands;

3.access the front-end Service react, go to "localhost:3000":
- go to "localhost:3000/technicians" to see the technicians list;

- go to "localhost:3000/technicians/new" to create a new technician;

- go to "localhost:3000/appointments" to see all the unfinished service appointments,click cancel/finish buttons to cancel/finish appointments;

- go to "localhost:3000/appointments/new": to create a new service appointment;

- go to "localhost:3000/apppointments/history": type in the VIN number to search the finished services.

4.to access the back-end service api, use "localhost:8080" and use the provided URLS to interact with the DB;

5.to access the front-end Inventory react:
- go to "localhost:3000/manufacturers" to see the list and add a new manufacturer for the dealership;

- go to "localhost:3000/vehicles" to see the list and add a new vehicle for the dealership;

- go to "localhost:3000/automobiles" to see the list and add a new automobile for the dealership;

6.to access the back-end Inventory api, use "localhost:8100" and use the provided URLS to interact with the DB.

7.app should now be accessible from the docker containers, api ports are 8090 for sales, 8080 for service, and 8100 for inventory.

## Application Diagram

![Service Diagram](Service-Diagram.png)
![Sales Diagram](Sales-Diagram.png)

## Services

**service: react**
> ports: -"3000:3000"
> urls:https://localhost:3000

**service: service-api**
> ports:- "8080:8000"
> urls: http://localhost:8080

**service: inventory-api**
> ports:- "8100:8000"
> urls:https://localhost:8100


**service:Sales api**
> port:- "8090:8090"
> urls:https://localhost:8090
> Sales api microservice that handles the Automobile Value Object, Potential_Customer, Salesperson, and Sale Record models
> Can create new Potential_Customer objects and list all customer objects
> Can create new Salesperson and list all salesperson objects
> Can create new Sale_Record models and list all sale records, can also list records by salesperson


## API Documentation

**service: service-api**
- for technician:

| Method | URL | What it does |
| --- | --- | --- |
| GET    | /api/technicians/ | Gets a list of all technicians|
| GET    | /api/technicians/<int:id>/                      | Gets the details of one instance of technicians    |
| POST   | /api/technicians/                               | Create a new instance of technician                  |
| DELETE | /api/technicians/<int:id>/                      | Deletes a single instance of technician               |

- for appointment:

| Method | URL | What it does |
| --- | --- | --- |
| GET | /api/appointments/ | Gets a list of all appointments|
| GET | /api/appointments/<int:id>/ | Gets the details of one instance of appointments |
| POST | /api/appointments/ | Create a new instance of appointment |
| DELETE | /api/appointments/<int:id>/ | Deletes a single instance of appointment |


**service: sales-api**

 - Sale Records:

| Method | URL | What it does |
| --- | --- | --- |
| GET    | /api/sale_records/                               | Gets a list of all sale records        |
| GET    | /api/sale_records/<int:id>/                      | Gets the details of one sale record    |
| POST   | /api/sale_records/                               | Create a new sale record instance      |
| DELETE | /api/sale_records/<int:id>/                      | Deletes a single sale record instance  |

- Salesperson:

| Method | URL | What it does |
| --- | --- | --- |
| GET    | /api/salespeople/                               | Gets a list of all salespeople                             |
| GET    | /api/salespeople/<int:pk>/                      | Gets the details of one salesperson                        |
| POST   | /api/salespeople/                               | Create a new salesperson instance                          |
| DELETE | /api/salespeople/<int:pk>/                      | Deletes a single salesperson instance                      |
| PUT    | /api/salespeople/<int:pk>/                      | Updates a single salesperson instance with inputted body   |

- Potential_Customer:

| Method | URL | What it does |
| --- | --- | --- |
| GET    | /api/customers/                               | Gets a list of all potential_customer instances                   |
| GET    | /api/customers/<int:pk>/                      | Gets the details of one potential_customer instance               |
| POST   | /api/customers/                               | Create a new potential_customer instance                          |
| DELETE | /api/customers/<int:pk>/                      | Deletes a single potential_customer instance                      |
| PUT    | /api/customers/<int:pk>/                      | Updates a single potential_customer instance with inputted body   |

- AutomobileVO:

| Method | URL | What it does |
| --- | --- | --- |
| GET    | /api/autos                                    | Gets a list of all AutomobileVO instances   |


## Value Objects

* AutomobileVO -> Service api based Value Object pulling the Automobile model DB from Inventory api (using the Vin mainly)

* AutomobileVO -> Sales api based Value Object pulling the Automobile model DB from Inventory api used in the SalesRecord model
