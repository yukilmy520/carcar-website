from common.json import ModelEncoder
from .models import AutomobileVO, SaleRecord, Salesperson, Potential_Customer

class AutomobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "import_href",
        "color",
        "year",
        "vin",
        "is_sold",


    ]

class SalespersonEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "name",
        "employee_num",
        "id",
    ]

class Potential_CustomerEncoder(ModelEncoder):
    model = Potential_Customer
    properties = [
        "customer_name",
        "address",
        "phone_number",
        "id",
    ]


class SaleRecordEncoder(ModelEncoder):
    model = SaleRecord
    properties = [
        "price",
        "automobile",
        "customer",
        "salesperson",
        "id",
    ]
    encoders = {
        "automobile": AutomobileVODetailEncoder(),
        "customer": Potential_CustomerEncoder(),
        "salesperson": SalespersonEncoder(),
    }
