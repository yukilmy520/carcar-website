from django.contrib import admin
from .models import SaleRecord, Salesperson, Potential_Customer


admin.site.register(Salesperson)
admin.site.register(SaleRecord)
admin.site.register(Potential_Customer)
