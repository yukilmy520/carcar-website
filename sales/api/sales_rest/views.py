from django.shortcuts import render
from .encoders import SalespersonEncoder, SaleRecordEncoder, Potential_CustomerEncoder
from .models import AutomobileVO, SaleRecord, Salesperson, Potential_Customer
from django.http import JsonResponse
import json
from django.views.decorators.http import require_http_methods
# Create your views here.


@require_http_methods(["GET", "POST"])
def api_salespeople(request):
    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople": salespeople},
            encoder=SalespersonEncoder,
        )
    else:
        content = json.loads(request.body)
        salesperson = Salesperson.objects.create(**content)
        return JsonResponse(
            salesperson,
            encoder=SalespersonEncoder,
            safe=False,
        )

@require_http_methods(["GET", "POST"])
def api_customers(request):
    if request.method == "GET":
        customers = Potential_Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=Potential_CustomerEncoder,
        )
    else:
        content = json.loads(request.body)
        customer = Potential_Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=Potential_CustomerEncoder,
            safe=False,
        )

@require_http_methods(["GET", "POST"])
def api_sale_records(request):
    if request.method == "GET":
        records = SaleRecord.objects.all()
        return JsonResponse(
            {"sale_records": records},
            encoder=SaleRecordEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            autos_href = content["automobile"]
            autos = AutomobileVO.objects.get(import_href=autos_href)
            content["automobile"] = autos
            salesperson_id = content["salesperson"]
            salesperson = Salesperson.objects.get(id=salesperson_id)
            content["salesperson"] = salesperson
            customer_id = content["customer"]
            customer = Potential_Customer.objects.get(id=customer_id)
            content["customer"] = customer
        except (AutomobileVO.DoesNotExist, Salesperson.DoesNotExist, Potential_Customer.DoesNotExist):
            return JsonResponse(
                {"message": "One or more items is Invalid!"},
                status=400
            )
        record = SaleRecord.objects.create(**content)
        return JsonResponse(
            record,
            encoder=SaleRecordEncoder,
            safe=False,
        )

@require_http_methods(["GET", "DELETE"])
def api_sale_record(request, id):
    if request.method == "GET":
        record = SaleRecord.objects.get(id=id)
        return JsonResponse(
        {"sale_record": record},
        encoder=SaleRecordEncoder,
        )
    else:
        try:
            record = SaleRecord.objects.get(id=id)
            record.delete()
            return JsonResponse(
                record,
                encoder=SalespersonEncoder,
                safe=False,
            )
        except SaleRecord.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})

@require_http_methods(["GET"])
def api_show_automobile_vos(request):
    auto_vos = AutomobileVO.objects.all()
    autos = []
    for a in auto_vos:
        autos.append({
            "href": a.import_href,
            "vin": a.vin,
            "color": a.color,
            "year": a.year,
            "is_sold": a.is_sold,

        })
    return JsonResponse({"autos": autos})


@require_http_methods(["DELETE", "GET", "PUT"])
def api_salesperson(request, pk):
    if request.method == "GET":
        try:
            salesperson = Salesperson.objects.get(id=pk)
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False
            )
        except Salesperson.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            salesperson = Salesperson.objects.get(id=pk)
            salesperson.delete()
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else: # PUT
        try:
            content = json.loads(request.body)
            salesperson = Salesperson.objects.get(id=pk)

            props = ["name", "employee_num"]
            for prop in props:
                if prop in content:
                    setattr(salesperson, prop, content[prop])
            salesperson.save()
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response

@require_http_methods(["DELETE", "GET", "PUT"])
def api_customer(request, pk):
    if request.method == "GET":
        try:
            customer = Potential_Customer.objects.get(id=pk)
            return JsonResponse(
                customer,
                encoder=Potential_CustomerEncoder,
                safe=False
            )
        except Potential_Customer.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            customer = Potential_Customer.objects.get(id=pk)
            customer.delete()
            return JsonResponse(
                customer,
                encoder=Potential_CustomerEncoder,
                safe=False,
            )
        except Potential_Customer.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else: # PUT
        try:
            content = json.loads(request.body)
            customer = Potential_Customer.objects.get(id=pk)

            props = ["customer_name", "address", "phone_number"]
            for prop in props:
                if prop in content:
                    setattr(customer, prop, content[prop])
            customer.save()
            return JsonResponse(
                customer,
                encoder=Potential_CustomerEncoder,
                safe=False,
            )
        except Potential_Customer.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
